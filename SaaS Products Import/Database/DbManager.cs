﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaaS_Products_Import.Database
{
    internal class DbManager
    {
        private static DbManager Instance;

        public static DbManager GetInstance()
        {
            lock (typeof(DbManager))
            {
                return Instance ?? (Instance = new DbManager());
            }
        }

        public void InsertProducts(List<SaaSProduct> products)
        {
            foreach (SaaSProduct product in products)
            {
                Console.WriteLine(product);
            }
        }
    }
}