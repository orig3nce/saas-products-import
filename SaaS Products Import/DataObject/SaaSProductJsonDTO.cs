﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaaS_Products_Import.DataObject
{
    internal class SaaSProductJsonDTO
    {
        [JsonProperty("products")]
        public List<SaaSProductDTO> Products { get; set; }
    }

    internal class SaaSProductYamlDTO
    {
        [JsonProperty("products")]
        public List<SaaSProductDTO> Products { get; set; }
    }
}