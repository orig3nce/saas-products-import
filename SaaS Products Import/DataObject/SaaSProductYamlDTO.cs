﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace SaaS_Products_Import
{
    internal class SaaSProductYamlDTO
    {
        [YamlMember(Alias = "name")]
        public string Name { get; set; }

        [YamlMember(Alias = "tags")]
        public string Categories { get; set; }

        [YamlMember(Alias = "twitter")]
        public string Twitter { get; set; }

        internal static List<SaaSProduct> ToDomain(List<SaaSProductYamlDTO> products)
        {
            List<SaaSProduct> result = new List<SaaSProduct>();
            foreach (SaaSProductYamlDTO product in products)
            {
                result.Add(ToDomain(product));
            }
            return result;
        }

        internal static SaaSProduct ToDomain(SaaSProductYamlDTO product)
        {
            return new SaaSProduct()
            {
                Name = product.Name,
                Categories = product.Categories.Split(",").ToList(),
                Twitter = product.Twitter
            };
        }
    }
}