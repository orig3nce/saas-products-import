﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaaS_Products_Import
{
    public class SaaSProductDTO
    {
        [JsonProperty("title")]
        public string Name { get; set; }

        [JsonProperty("categories")]
        public List<string> Categories { get; set; }

        [JsonProperty("twitter")]
        public string Twitter { get; set; }

        public static List<SaaSProduct> ToDomain(List<SaaSProductDTO> products)
        {
            List<SaaSProduct> result = new List<SaaSProduct>();
            foreach (SaaSProductDTO product in products)
            {
                result.Add(ToDomain(product));
            }
            return result;
        }

        public static SaaSProduct ToDomain(SaaSProductDTO product)
        {
            return new SaaSProduct()
            {
                Name = product.Name,
                Categories = product.Categories,
                Twitter = product.Twitter
            };
        }
    }
}