﻿using Newtonsoft.Json;
using SaaS_Products_Import.Database;
using SaaS_Products_Import.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace SaaS_Products_Import.Import
{
    internal class ImportManager
    {
        private const string ImportPath = "../../../../feed-products/";
        private const string ImportUrl = "";

        public static void StartImport()
        {
            ImportFromFiles();
            ImportFromUrl();
        }

        private static void ImportFromFiles()
        {
            // Obtain files from import directory
            string[] fileEntries = Directory.GetFiles(ImportPath);
            foreach (string fileName in fileEntries)
            {
                string[] fileNameSplit = fileName.Split('.');
                string? fileType = fileNameSplit.LastOrDefault();

                if (fileType != null)
                {
                    switch (fileType)
                    {
                        case "yaml":
                            Console.WriteLine($"import {fileName}");
                            ImportYamlFile(fileName);
                            break;

                        case "json":
                            Console.WriteLine($"import {fileName}");
                            ImportJsonFile(fileName);
                            break;

                        case "csv":
                            Console.WriteLine($"{fileName} cannot be imported. CSV files not implemented");
                            break;
                    }
                }
                else Console.WriteLine($"{fileName} cannot be imported. Invalid file type.");
            }
        }

        private static void ImportFromUrl()
        {
            if (string.IsNullOrEmpty(ImportUrl)) return;
            //TODO http request to download csv string
        }

        private static void ImportJsonFile(string fileName)
        {
            SaaSProductJsonDTO productsDTO = JsonConvert.DeserializeObject<SaaSProductJsonDTO>(File.ReadAllText(fileName));
            if (productsDTO != null)
            {
                List<SaaSProduct> products = SaaSProductDTO.ToDomain(productsDTO.Products);
                DbManager.GetInstance().InsertProducts(products);
            }
        }

        private static void ImportYamlFile(string fileName)
        {
            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(UnderscoredNamingConvention.Instance)
                .Build();

            List<SaaSProductYamlDTO> productsDTO = deserializer.Deserialize<List<SaaSProductYamlDTO>>(File.ReadAllText(fileName));

            if (productsDTO != null)
            {
                List<SaaSProduct> products = SaaSProductYamlDTO.ToDomain(productsDTO);
                DbManager.GetInstance().InsertProducts(products);
            }
        }

        private static void ImportCsvString(string csvData)
        {
            throw new NotImplementedException();
        }
    }
}