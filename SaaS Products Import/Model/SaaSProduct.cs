﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaaS_Products_Import
{
    public class SaaSProduct : ProductBase
    {
        public string Twitter { get; set; }

        public override string ToString()
        {
            return $"Name: \"{Name}\"; Categories: {string.Join(", ", Categories)}; Twitter:{Twitter}";
        }
    }
}