﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaaS_Products_Import
{
    public class ProductBase
    {
        public string Name { get; set; }

        public List<string> Categories { get; set; }
    }
}