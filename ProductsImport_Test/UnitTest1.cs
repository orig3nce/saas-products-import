﻿using Newtonsoft.Json;
using SaaS_Products_Import;
using SaaS_Products_Import.Database;
using SaaS_Products_Import.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ProductsImport_Test
{
    public class UnitTest1
    {
        [Fact]
        public void ShouldConvertSaaSProductDTOToDomain()
        {
            var dto = new SaaSProductDTO()
            {
                Name = "Test Name",
                Categories = new List<string> { "abcdef", "random", "xyzwordle" },
                Twitter = "@gimmefollow"
            };

            var product = SaaSProductDTO.ToDomain(dto);

            Assert.Equal(dto.Name, product.Name);
            Assert.Equal(dto.Categories, product.Categories);
            Assert.Equal(dto.Twitter, product.Twitter);
        }

        [InlineData("MyTweetio")]
        [InlineData("AÀÁÄ三_-_水")]
        [InlineData(null)]
        [InlineData("")]
        [Theory]
        public void ShouldConvertDTOToDomainDifferentTwitter(string twitter)
        {
            var dto = new SaaSProductDTO()
            {
                Name = "Test Name",
                Categories = new List<string> { "abcdef", "random", "xyzwordle" },
                Twitter = twitter
            };

            var product = SaaSProductDTO.ToDomain(dto);

            Assert.Equal(dto.Name, product.Name);
            Assert.Equal(dto.Categories, product.Categories);
            Assert.Equal(dto.Twitter, product.Twitter);
        }

        [InlineData("MyTweetio", "", "Groceries")]
        [InlineData("AÀÁÄ三_-_水", "Data vsiualization", ".")]
        [InlineData("*", "", "Hi")]
        [InlineData("Real Time messaging", "Productivity", "Holidays booking")]
        [Theory]
        public void ShouldShowProductToString(string category1, string category2, string category3)
        {
            var product = new SaaSProduct()
            {
                Name = "Test Name",
                Categories = new List<string> { category1, category2, category3 },
                Twitter = "@tweet"
            };

            Assert.Contains(category1, product.ToString());
            Assert.Contains(category2, product.ToString());
            Assert.Contains(category3, product.ToString());
        }
    }
}