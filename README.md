### Coding Assignment

- Installation steps
    - Download or clone repository
    - Run it from visual studio or go to the path: “saas-products-import-main\saas-products-import-main\SaaS Products Import\bin\Debug\net6.0” 
    and execute file: “SaaS Products Import.exe”
    
- How to run your code / tests
    - The project and test project can be found in the same repository.
        - Project name: SaaS Products Import
        - Test project name: ProductsImport_Test
    - The project and tests can be run using IDE Visual Studio. The exact version being used is:
        - Microsoft Visual Studio Community 2022 (64-bit) - Current
        Version 17.1.4
    - A couple external libraries have been added to the project with NuGet Package Manager
    - JSON library: Newtonsoft.Json
    - YAML library: YamlDotNet
- Where to find your code
    - Repository can be found at: [https://gitlab.com/orig3nce/saas-products-import](https://gitlab.com/orig3nce/saas-products-import)
- Was it your first time writing a unit test, using a particular framework, etc?
    - It has been first time using a private repository with gitlab. Actually, it gave me some trouble to set up authentication for being able to push to origin.
- What would you have done differently if you had had more time
    - I have seen is a challenge to implement unit tests when most of the code is input/output from different sources. There’s more work in setting up classes to deserialize/serialize than actual code logic.
    - I would have liked to setup the project in a more microservice oriented architecture with dependency injection for config files, database, import managers, etc.
    - Add config files for environment variables, url, filepath, etc.
    - More consistency in null checking
    - I have been in the mindset that this is a proof of concept or minimum viable product approach. Before starting more important developments, we must think about the entire structure of the project and the relationship between the database, http calls, file managers...